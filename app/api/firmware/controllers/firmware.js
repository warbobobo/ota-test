'use strict';

const { sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async latsetVer(ctx) {
    if (!ctx.query.name) {
      ctx.status = 404;
      ctx.body = 'not found';
      return;
    } 
    return await strapi.services.firmware.findOne({
      BuildName: ctx.query.name,
      _sort: 'Version:desc'
    });
  }
};
