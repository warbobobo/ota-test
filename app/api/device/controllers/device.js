'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async updateStatus(ctx) {
    const { id } = await strapi.services.device.findOne({ Sn: ctx.params.Sn });
    return await strapi.services.device.update({ id }, ctx.request.body);
  }
};
